INSERT INTO [OpenNos].[dbo].[RollGeneratedItem] (
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[IsRareRandom],
	[MinimumOriginalItemRare],
	[MaximumOriginalItemRare]
)
VALUES
	(9, 302, 10, 1, 4001, 1, 0, 7),
	(9, 302, 10, 1, 4003, 1, 0, 7),
	(9, 302, 10, 1, 4005, 1, 0, 7),
	(9, 302, 10, 1, 4007, 1, 0, 7),
	(9, 302, 10, 1, 4009, 1, 0, 7),
	(9, 302, 10, 1, 4011, 1, 0, 7),
	(9, 302, 10, 1, 4013, 1, 0, 7),
	(9, 302, 10, 1, 4016, 1, 0, 7),
	(9, 302, 10, 1, 4019, 1, 0, 7),
	(9, 302, 5, 1, 4036, 1, 0, 7),
	(9, 302, 5, 1, 4037, 1, 0, 7),
	(9, 302, 5, 1, 4036, 1, 0, 7),
	(9, 302, 5, 1, 4040, 1, 0, 7),
	(9, 302, 5, 1, 4041, 1, 0, 7),
	(9, 302, 5, 1, 4042, 0, 0, 0),
	(9, 302, 5, 1, 4045, 1, 0, 7);