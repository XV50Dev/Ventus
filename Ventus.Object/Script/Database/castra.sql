INSERT INTO [OpenNos].[dbo].[RollGeneratedItem] (
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[IsRareRandom],
	[MinimumOriginalItemRare],
	[MaximumOriginalItemRare]
)
VALUES
	(2, 302, 15, 1, 312, 1, 0, 7),
	(2, 302, 15, 1, 313, 1, 0, 7),
	(2, 302, 15, 1, 314, 1, 0, 7),
	(2, 302, 12, 1, 318, 1, 0, 7),
	(2, 302, 12, 1, 320, 1, 0, 7),
	(2, 302, 7, 1, 263, 1, 0, 7),
	(2, 302, 7, 1, 266, 1, 0, 7),
	(2, 302, 7, 1, 269, 1, 0, 7),
	(2, 302, 1, 1, 2516, 1, 0, 7),
	(2, 302, 7, 1, 909, 1, 0, 7),
	(2, 302, 7, 1, 911, 1, 0, 7),
	(2, 302, 7, 1, 913, 1, 0, 7);