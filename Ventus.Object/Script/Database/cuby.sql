INSERT INTO [OpenNos].[dbo].[RollGeneratedItem] (
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[IsRareRandom],
	[MinimumOriginalItemRare],
	[MaximumOriginalItemRare]
)
VALUES
	(0, 302, 10, 1, 265, 1, 0, 7),
	(0, 302, 10, 1, 262, 1, 0, 7),
	(0, 302, 10, 1, 268, 1, 0, 7),
	(0, 302, 10, 1, 315, 1, 0, 7),
	(0, 302, 10, 1, 321, 1, 0, 7),
	(0, 302, 5, 1, 2282, 1, 0, 7),
	(0, 302, 5, 1, 1030, 1, 0, 7),
	(0, 302, 1, 1, 2514, 1, 0, 7),
	(0, 302, 1, 1, 2515, 1, 0, 7),
	(0, 302, 8, 4, 1020, 1, 0, 7),
	(0, 302, 9, 1, 311, 1, 0, 7),
	(0, 302, 9, 1, 309, 1, 0, 7),
	(0, 302, 4, 1, 900, 1, 0, 7),
	(0, 302, 4, 1, 907, 1, 0, 7),
	(0, 302, 4, 1, 908, 1, 0, 7),
	(0, 302, 4, 1, 4099, 1, 0, 7),
	(0, 302, 9, 1, 310, 1, 0, 7);