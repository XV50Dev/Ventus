INSERT INTO [OpenNos].[dbo].[RollGeneratedItem] (
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[IsRareRandom],
	[MinimumOriginalItemRare],
	[MaximumOriginalItemRare]
)
VALUES
	(16, 302, 7, 1, 4500, 1, 0, 7),
	(16, 302, 7, 1, 4501, 1, 0, 7),
	(16, 302, 7, 1, 4502, 1, 0, 7),
	(16, 302, 16, 10, 2519, 1, 0, 0),
	(16, 302, 10, 10, 2518, 1, 0, 0),
	(16, 302, 10, 50, 2282, 1, 0, 0),
	(16, 302, 10, 30, 1030, 1, 0, 0),
	(16, 302, 10, 15, 2349, 1, 0, 0),
	(16, 302, 10, 10, 2511, 1, 0, 0),
	(16, 302, 10, 10, 2512, 1, 0, 0),
	(16, 302, 10, 10, 2513, 1, 0, 0);
