#include "mywidget.h"
#include "ui_mywidget.h"

MyWidget::MyWidget(QWidget *parent)
    : QWidget(parent), ui(new Ui::MyWidget)
{
    ui->setupUi(this);

    vBoxLayout = new QVBoxLayout(this);
    layout()->setMenuBar((menuBar = new QMenuBar()));
}

MyWidget::~MyWidget()
{
    delete menuBar;
    delete vBoxLayout;
    delete ui;
}

QMenuBar *MyWidget::get() {
    return menuBar;
}

QMenu *MyWidget::addMenu(const QString &title) {
    return menuBar->addMenu(title);
}
