#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QMenuBar>

namespace Ui {
    class MyWidget;
}

class MyWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MyWidget(QWidget *parent = 0);
    ~MyWidget();

    QMenuBar *get();
    QMenu *addMenu(const QString& title);

private:
    Ui::MyWidget *ui;

    QVBoxLayout *vBoxLayout;
    QMenuBar *menuBar;

};

#endif // MYWIDGET_H
