#include "nosmalleditor.h"
#include "NosTaleStringDeEncrypt.hpp"
#include <iostream>
#include <Windows.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <qfiledialog.h>

#include <QPixmap>
#include <QMessageBox>

#include "mywidget.h"

std::string NumberToString(int Number)
{
	std::ostringstream ss;
	ss << Number;
	return ss.str();
}


NosmallEditor::NosmallEditor(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	ui.plainTextEdit->hide();

	//SetUp Menu

	menu = ui.widget->addMenu("File");
	Load = menu->addAction("Load");
	Save = menu->addAction("Save File");
	CreateList = menu->addAction("Create Item List");

	ui.widget->get()->setStyleSheet("QMenuBar { border-bottom: 1px solid black; }");
	//Connect
	connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(ChangedSelected()));
	connect(ui.pushButton_3, SIGNAL(clicked()), this, SLOT(ButtonClose()));
	connect(ui.pushButton_4, SIGNAL(clicked()), this, SLOT(ButtonMinimize()));
	connect(ui.lineEdit_5, SIGNAL(textChanged(const QString &)), this, SLOT(Search()));
	connect(Load, SIGNAL(triggered(bool)), this, SLOT(GetNostaleDataPath()));
	connect(CreateList, SIGNAL(triggered(bool)), this, SLOT(SaveList()));
	connect(Save, SIGNAL(triggered(bool)), this, SLOT(SaveDat()));
	connect(ui.pushButton, SIGNAL(clicked()), this, SLOT(NameChanged()));

	//Remove Border
	this->setWindowFlags(Qt::FramelessWindowHint);

	

	//SetUp LCD Number
	ui.lineEdit_4->setValidator(new QIntValidator(0, 999999, this));
	ui.lcdNumber->display(ui.treeWidget->topLevelItemCount());
	
}

NosmallEditor::~NosmallEditor()
{
	delete CreateList;
	delete Save;
	delete Load;

	delete menu;
}

std::vector<std::string> Split(const std::string str, char delim) {
	std::vector<std::string> tmp;
	std::stringstream ss(str);
	std::string item;
	while (std::getline(ss, item, delim)) { tmp.push_back(item); }
	return tmp;
}

void NosmallEditor::GetNostaleDataPath()
{
	NosTaleDataPath = QFileDialog::getOpenFileName(this, tr("Choose Nosmall Item File"), "C:\\","*.dat").toStdString();
	
	std::size_t found = NosTaleDataPath.find("nosmall.dat");
	if (found != std::string::npos)
	{

		LoadNosmallFile();
	}
	else
	{
		GetNostaleDataPath();
		return;
	}
}
void NosmallEditor::SaveList()
{
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->Id == ui.lineEdit->text().toInt()) {
			NTItem[i]->Activated = !ui.checkBox_3->isChecked();
			NTItem[i]->Price = ui.spinBox->value();
			NTItem[i]->isHot = ui.checkBox->isChecked();
			NTItem[i]->isNew = ui.checkBox_2->isChecked();
			NTItem[i]->Name = ui.lineEdit_6->text().toStdString();
			NTItem[i]->Description = ui.lineEdit_2->text().toStdString();
			NTItem[i]->ItemDescription = ui.lineEdit_3->text().toStdString();
		}
	}
	std::stringstream ss;
	ss << "ID\tVNum\tAmount\tPrice\n";
	for (int i = 0; i < NTItem.size(); i++) {
		if (!NTItem[i]->Activated) {
			ss << NTItem[i]->Id << "\t" << NTItem[i]->ItemVnum << "\t" << NTItem[i]->Amount << "\t" << NTItem[i]->Price << "\n";
		}
	}
	std::fstream fs;
	fs.open("List.txt", std::fstream::out | std::ofstream::trunc);
	fs << ss.str();
	fs.close();

	std::stringstream ss1;
	ss1 << "TRUNCATE TABLE dbo.Mall\nGO\n";
	ss1 << "SET IDENTITY_INSERT dbo.Mall ON\nGO\n";
	for (int i = 0; i < NTItem.size(); i++) {
		if (!NTItem[i]->Activated) {
			ss1 << "INSERT INTO dbo.Mall(Id,ItemVnum,Amount,Price) VALUES(" << NTItem[i]->Id << ", " << NTItem[i]->ItemVnum << ", " << NTItem[i]->Amount << ", " << NTItem[i]->Price << ")\nGO\n";
		}
	}
	ss1 << "SET IDENTITY_INSERT dbo.Mall OFF\nGO";
	std::fstream fs1;
	fs1.open("Mall.sql", std::fstream::out | std::ofstream::trunc);
	fs1 << ss1.str();
	fs1.close();

}
void NosmallEditor::SaveDat()
{
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->Id == ui.lineEdit->text().toInt()) {
			NTItem[i]->Activated = !ui.checkBox_3->isChecked();
			NTItem[i]->Show = ui.checkBox_4->isChecked();
			NTItem[i]->Price = ui.spinBox->value();
			NTItem[i]->isHot = ui.checkBox->isChecked();
			NTItem[i]->isNew = ui.checkBox_2->isChecked();
			NTItem[i]->Name = ui.lineEdit_6->text().toStdString();
			NTItem[i]->Description = ui.lineEdit_2->text().toStdString();
			NTItem[i]->ItemDescription = ui.lineEdit_3->text().toStdString();
		}
	}
	std::stringstream ss;
	
	foreach(NosmallItem *item, NTItem) {
		ss << "VNUM\t" << item->Id << "\t" << item->VNUM_1 << "\t" << item->isNew << "\t" << item->isHot << "\t" << item->VNUM_2 << "\t" << item->Activated << "\t" << item->Show << "\x0D"
			<< "ITEM\t" << item->ITEM_0 << "\t" << item->ITEM_1 << "\t" << item->ItemVnum << "\t" << item->PictureID << "\t" << item->ITEM_2 << "\t" << item->ITEM_3 << "\x0D"
			<< "ID\t" << item->ID_NUM << "\x0D"
			<< "TITLE1\t" << item->Name << "\x0D"
			<< "TITLE2\t" << item->Description << "\x0D"
			<< "COST\t" << item->Price << "\t" << item->COST_0 << "\t" << item->COST_1 << "\t" << item->Amount << "\t" << item->COST_3 << "\t" << item->Duration << "\x0D"
			<< "LINK\t" << item->Count << "\t" << item->ITEM_ID_1 << "\t" << item->ITEM_ID_2 << "\t" << item->ITEM_ID_3 << "\t" << item->ITEM_ID_4 << "\t" << item->ITEM_ID_5 << "\x0D"
			<< "DSTART\x0D"
			<< item->ItemDescription << "\x0D"
			<< "DEND\x0D"
			<< "END\x0D"
			<< "#==============================================\x0D\x0D";
	}
	std::fstream fs;
	fs.open("_nosmall.dat", std::fstream::out | std::ofstream::trunc);
	fs << ss.str();
	fs.close();
}
bool to_bool(std::string const& s) {
	return s != "0";
}

void NosmallEditor::LoadNosmallFile()
{
	ui.treeWidget->clear();
	NTItem.clear();
	try
	{
		
		std::ifstream fstream(NosTaleDataPath);
		NosmallItem *nosmallitem = new NosmallItem;
		bool started = false;
		bool bstart = false;
		std::vector<std::string> line;
		if (fstream.is_open()) {
			while (fstream.good())
			{
				for (std::string _line; std::getline(fstream, _line,'\r');) {
					if (_line == "#==============================================" || _line == "" || _line == "\r" || _line == "\n")
						continue;
					line = Split(_line, '\t');
					if (line.size() != 0)
					{
						if (_line == "TITLE1	")
							continue;
						if (line[0] == "VNUM") {
							if (started) {
								MessageBoxA(NULL, "ERROR ON LOAD ITEMS", "ERROR", MB_OK);
							}
							else
								started = true;
							nosmallitem->Id = std::atoi(line[1].c_str());
							nosmallitem->VNUM_1 = std::atoi(line[2].c_str());
							nosmallitem->isNew = std::atoi(line[3].c_str());
							nosmallitem->isHot = std::atoi(line[4].c_str());
							nosmallitem->VNUM_2 = std::atoi(line[5].c_str());
							nosmallitem->Activated = std::atoi(line[6].c_str());
							nosmallitem->Show = std::atoi(line[7].c_str());
						}
						if (line[0] == "ITEM") {
							nosmallitem->ITEM_0 = std::atoi(line[1].c_str());
							nosmallitem->ITEM_1 = std::atoi(line[2].c_str());
							nosmallitem->ItemVnum = line[3].c_str();
							nosmallitem->PictureID = std::atoi(line[4].c_str());
							nosmallitem->ITEM_2 = std::atoi(line[5].c_str());
							nosmallitem->ITEM_3 = std::atoi(line[6].c_str());
						}
						if (line[0] == "ID") {
							nosmallitem->ID_NUM = line[1];
						}
						if (line[0] == "TITLE1") {
							nosmallitem->Name = line[1];
						}
						if (line[0] == "TITLE2") {
							nosmallitem->Description = line[1];
						}
						if (line[0] == "COST") {
							nosmallitem->Price = std::atoi(line[1].c_str());
							nosmallitem->COST_0 = std::atoi(line[2].c_str());
							nosmallitem->COST_1 = std::atoi(line[3].c_str());
							nosmallitem->Amount = std::atoi(line[4].c_str());
							nosmallitem->COST_3 = std::atoi(line[5].c_str());
							nosmallitem->Duration = std::atoi(line[6].c_str());
						}
						if (line[0] == "LINK") {
							nosmallitem->Count = std::atoi(line[1].c_str());
							nosmallitem->ITEM_ID_1 = line[2].c_str();
							nosmallitem->ITEM_ID_2 = line[3].c_str();
							nosmallitem->ITEM_ID_3 = line[4].c_str();
							nosmallitem->ITEM_ID_4 = line[5].c_str();
							nosmallitem->ITEM_ID_5 = line[6].c_str();
						}
						if (line[0] == "DSTART") {
							bstart = true;
						}
						if (line[0] == "DEND") {
							bstart = false;
						}
						if (bstart) {
							nosmallitem->ItemDescription = line[0];
						}
						if (line[0] == "END")
						{
							if (started)
							{
								NTItem.push_back(nosmallitem);
								nosmallitem = new NosmallItem;
								started = false;
							}
							else
								MessageBoxA(NULL, "ERROR ON LOAD ITEMS", "ERROR", MB_OK);

						}
					}
				}
				
			}
		}
		else
		{
			MessageBoxA(NULL, "Cant Open the File!", "ERROR", MB_OK);
		}
		LoadNosmallItems();
		fstream.close();
	}
	catch (std::bad_alloc ex) {

		LoadNosmallItems();
		MessageBoxA(NULL, ex.what(), "Bad Alloc Error", NULL);
	}
	catch (std::exception ex)
	{
		LoadNosmallItems();
		MessageBoxA(NULL, ex.what(), "Exception Error", NULL);
	}
	catch (std::overflow_error overflow)
	{
		LoadNosmallItems();
		MessageBoxA(NULL, overflow.what(), "Overflow Exception Error", NULL);
	}
	catch (...)
	{
		MessageBoxA(NULL, "ERROR ON LOAD ITEMS", "ERROR", MB_OK);
	}
}

void NosmallEditor::LoadNosmallItems()
{
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->ID_NUM == "-1") {
			for (int a = 0; a < NTItem.size(); a++) {
				if (NumberToString(NTItem[a]->Id) == NTItem[i]->ITEM_ID_1) {
					NTItem[a]->isSub = true;
				}
				else if (NumberToString(NTItem[a]->Id) == NTItem[i]->ITEM_ID_2) {
					NTItem[a]->isSub = true;
				}
				else if (NumberToString(NTItem[a]->Id) == NTItem[i]->ITEM_ID_3) {
					NTItem[a]->isSub = true;
				}
				else if (NumberToString(NTItem[a]->Id) == NTItem[i]->ITEM_ID_4) {
					NTItem[a]->isSub = true;
				}
				else if (NumberToString(NTItem[a]->Id) == NTItem[i]->ITEM_ID_5) {
					NTItem[a]->isSub = true;
				}
			}
		}
	}
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->isSub)
			continue;
		QTreeWidgetItem *itemo = new QTreeWidgetItem;
		QString text = NumberToString(NTItem[i]->Id).c_str();
		itemo->setText(0, text);
		QTreeWidgetItem *itemo2(itemo);
		if (NTItem[i]->ID_NUM == "-1") {
			itemo->parent();
			itemo2 = new QTreeWidgetItem;
			itemo2->setTextAlignment(0, Qt::AlignCenter);
			if (NTItem[i]->ITEM_ID_1 != "-1") {
				itemo2->setText(0, NTItem[i]->ITEM_ID_1.c_str());
				itemo->addChild(itemo2);
			}
			itemo2 = new QTreeWidgetItem;
			itemo2->setTextAlignment(0, Qt::AlignCenter);
			if (NTItem[i]->ITEM_ID_2 != "-1") {
				itemo2->setText(0, NTItem[i]->ITEM_ID_2.c_str());
				itemo->addChild(itemo2);
			}
			itemo2 = new QTreeWidgetItem;
			itemo2->setTextAlignment(0, Qt::AlignCenter);
			if (NTItem[i]->ITEM_ID_3 != "-1") {
				itemo2->setText(0, NTItem[i]->ITEM_ID_3.c_str());
				itemo->addChild(itemo2);
			}
			itemo2 = new QTreeWidgetItem;
			itemo2->setTextAlignment(0, Qt::AlignCenter);
			if (NTItem[i]->ITEM_ID_4 != "-1") {
				itemo2->setText(0, NTItem[i]->ITEM_ID_4.c_str());
				itemo->addChild(itemo2);
			}
			itemo2 = new QTreeWidgetItem;
			itemo2->setTextAlignment(0, Qt::AlignCenter);
			if (NTItem[i]->ITEM_ID_5 != "-1") {
				itemo2->setText(0, NTItem[i]->ITEM_ID_5.c_str());
				itemo->addChild(itemo2);
			}
		}

		ui.treeWidget->addTopLevelItem(itemo);
	}
	ui.lcdNumber->display(ui.treeWidget->topLevelItemCount());
}
std::string getexepath()
{
	char result[MAX_PATH];
	return std::string(result, GetModuleFileNameA(NULL, result, MAX_PATH));
}

std::string ReplaceString(std::string subject, const std::string& search, const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}

void NosmallEditor::ChangedSelected()
{
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->Id == ui.lineEdit->text().toInt()) {
			NTItem[i]->Activated = !ui.checkBox_3->isChecked();
			NTItem[i]->Show = ui.checkBox_4->isChecked();
			NTItem[i]->Price = ui.spinBox->value();
			NTItem[i]->isHot = ui.checkBox->isChecked();
			NTItem[i]->isNew = ui.checkBox_2->isChecked();
			NTItem[i]->Name = ui.lineEdit_6->text().toStdString();
			NTItem[i]->Description = ui.lineEdit_2->text().toStdString();
			NTItem[i]->ItemDescription = ui.lineEdit_3->text().toStdString();
		}
	}
	ui.lineEdit->setText(ui.treeWidget->currentItem()->text(ui.treeWidget->currentColumn()));
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->Id == ui.lineEdit->text().toInt()) {
			ui.lineEdit_2->setText(QString(NTItem[i]->Description.c_str()));
			ui.lineEdit_3->setText(QString(NTItem[i]->ItemDescription.c_str()));
			ui.spinBox->setValue(NTItem[i]->Price);
			ui.spinBox_2->setValue(NTItem[i]->Amount);
			ui.comboBox->setCurrentIndex(NTItem[i]->ITEM_3);
			ui.checkBox->setChecked(NTItem[i]->isHot);
			ui.checkBox_2->setChecked(NTItem[i]->isNew);
			ui.checkBox_3->setChecked(!NTItem[i]->Activated);
			ui.checkBox_4->setChecked(NTItem[i]->Show);
			ui.checkBox_5->setChecked(NTItem[i]->isSub);
			ui.lineEdit_4->setText(QString(NTItem[i]->ItemVnum.c_str()));
			ui.lineEdit_6->setText(QString(NTItem[i]->Name.c_str()));
			ui.lineEdit_7->setText(QString(NumberToString(NTItem[i]->Duration).c_str()));
			std::string test;
			test = ReplaceString(getexepath(),"\\Nosmall Editor.exe","");
			test += "\\images\\";
			test += NumberToString(NTItem[i]->PictureID).c_str();
			test += ".png";
			QImage *pic = new QImage(test.c_str(), "PNG");

			if (pic->isNull())
			{
				QMessageBox::information(this,
					tr("Nosmall Editor Error"),
					tr("Cannot load %1.").arg(test.c_str()));
				return;
			}
			ui.label_7->setBackgroundRole(QPalette::Base);
			ui.label_7->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
			ui.label_7->setScaledContents(true);
			ui.label_7->setPixmap(QPixmap::fromImage(*pic));
		}
	}
}



void NosmallEditor::NameChanged()
{
	QList<QTreeWidgetItem*> clist;
	for (int i = 0; i < NTItem.size(); i++) {
		if (!NTItem[i]->Activated) {
			clist.append((ui.treeWidget->findItems(NumberToString(NTItem[i]->Id).c_str(), Qt::MatchRecursive, 0)));
		}
	}

	/*
	QList<QTreeWidgetItem*> clist = ui.treeWidget->findItems(ui.lineEdit_5->text(), Qt::MatchContains | Qt::MatchRecursive, 0);
	*/
	QList<QTreeWidgetItem*> items = ui.treeWidget->findItems("", Qt::MatchContains | Qt::MatchRecursive, 0);
	foreach(QTreeWidgetItem* item, items)
	{
		ui.treeWidget->setItemHidden(item, true);
	}
	foreach(QTreeWidgetItem* item, clist)
	{
		ui.treeWidget->setItemHidden(item, false);
	}
}

void NosmallEditor::Search()
{
	QList<QTreeWidgetItem*> clist;
	for (int i = 0; i < NTItem.size(); i++) {
		if (NTItem[i]->ItemVnum == (ui.lineEdit_5->text().toStdString().c_str())) {
			clist.append((ui.treeWidget->findItems(NumberToString(NTItem[i]->Id).c_str(), Qt::MatchRecursive, 0)));
		}
	}
	if (clist.size() == 0)
		clist = ui.treeWidget->findItems(ui.lineEdit_5->text(), Qt::MatchContains | Qt::MatchRecursive, 0);
	/*
	QList<QTreeWidgetItem*> clist = ui.treeWidget->findItems(ui.lineEdit_5->text(), Qt::MatchContains | Qt::MatchRecursive, 0);
	*/
	QList<QTreeWidgetItem*> items = ui.treeWidget->findItems("", Qt::MatchContains | Qt::MatchRecursive, 0);
	foreach(QTreeWidgetItem* item, items)
	{
		ui.treeWidget->setItemHidden(item, true);
	}
	foreach(QTreeWidgetItem* item, clist)
	{
		ui.treeWidget->setItemHidden(item, false);
	}
}



void NosmallEditor::mouseMoveEvent(QMouseEvent *event)
{
	if (event->buttons() & Qt::LeftButton) {
		move(event->globalPos() - dragPosition);
		event->accept();
	}
}


void NosmallEditor::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		dragPosition = event->globalPos() - frameGeometry().topLeft();
		event->accept();
	}
}


//Close Form
void NosmallEditor::ButtonClose()
{
	QApplication::quit();
}


//Minimize Form
void NosmallEditor::ButtonMinimize()
{
	this->setWindowState(Qt::WindowMinimized);
}