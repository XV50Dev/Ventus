/********************************************************************************
** Form generated from reading UI file 'nosmalleditor.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOSMALLEDITOR_H
#define UI_NOSMALLEDITOR_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>
#include <mywidget.h>

QT_BEGIN_NAMESPACE

class Ui_NosmallEditorClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *action;
    QAction *actionReload;
    QAction *actionLoad_Nosmall;
    QAction *action_3;
    QAction *actionTest;
    QAction *actionLoad;
    QAction *actionSave_2;
    QAction *actionLoad_2;
    QAction *actionSave_3;
    QAction *actiontest;
    QWidget *centralWidget;
    QTreeWidget *treeWidget;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label_2;
    QSpinBox *spinBox;
    QLabel *label_3;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QComboBox *comboBox;
    QLabel *label_4;
    QLineEdit *lineEdit_3;
    QLCDNumber *lcdNumber;
    QLabel *label_5;
    QLabel *label_6;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QLineEdit *lineEdit_5;
    MyWidget *widget;
    QCheckBox *checkBox_3;
    QPlainTextEdit *plainTextEdit;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_6;
    QLabel *label_7;
    QLineEdit *lineEdit_7;
    QLabel *label;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QSpinBox *spinBox_2;
    QCheckBox *checkBox_4;
    QPushButton *pushButton;
    QCheckBox *checkBox_5;

    void setupUi(QMainWindow *NosmallEditorClass)
    {
        if (NosmallEditorClass->objectName().isEmpty())
            NosmallEditorClass->setObjectName(QString::fromUtf8("NosmallEditorClass"));
        NosmallEditorClass->setWindowModality(Qt::WindowModal);
        NosmallEditorClass->setEnabled(true);
        NosmallEditorClass->resize(601, 369);
        NosmallEditorClass->setMinimumSize(QSize(600, 300));
        NosmallEditorClass->setMaximumSize(QSize(9999, 9999));
        NosmallEditorClass->setBaseSize(QSize(600, 300));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/NosmallEditor/Unbenannt.png"), QSize(), QIcon::Normal, QIcon::Off);
        NosmallEditorClass->setWindowIcon(icon);
        NosmallEditorClass->setStyleSheet(QString::fromUtf8("QToolTip\n"
"{\n"
"     border: 1px solid black;\n"
"     background-color: #ffa02f;\n"
"     padding: 1px;\n"
"     border-radius: 3px;\n"
"     opacity: 100;\n"
"}\n"
"QWidget\n"
"{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"    \n"
"}\n"
"QWidget:item:hover\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #ca0619);\n"
"    color: #000000;\n"
"}\n"
"\n"
"QWidget:item:selected\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QMenuBar::item\n"
"{\n"
"    background: transparent;\n"
"}\n"
"\n"
"QMenuBar::item:selected\n"
"{\n"
"    background: transparent;\n"
"    border: 1px solid #ffaa00;\n"
"}\n"
"\n"
"QMenuBar::item:pressed\n"
"{\n"
"    background: #444;\n"
"    border: 1px solid #000;\n"
"    background-color: QLinearGradient(\n"
"        x1:0, y1:0,\n"
"        x2:0, y2:1,\n"
"        stop:1 #212121,\n"
"        stop:0.4 #343434/*,\n"
"        stop:0.2 #34"
                        "3434,\n"
"        stop:0.1 #ffaa00*/\n"
"    );\n"
"    margin-bottom:-1px;\n"
"    padding-bottom:1px;\n"
"}\n"
"\n"
"QMenu\n"
"{\n"
"    border: 1px solid #000;\n"
"}\n"
"QMenuBar \n"
"{\n"
"}\n"
"\n"
"QMenu::item\n"
"{\n"
"    padding: 2px 20px 2px 20px;\n"
"}\n"
"\n"
"QMenu::item:selected\n"
"{\n"
"    color: #000000;\n"
"}\n"
"\n"
"QWidget:disabled\n"
"{\n"
"    color: #404040;\n"
"    background-color: #323232;\n"
"}\n"
"\n"
"QAbstractItemView\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #4d4d4d, stop: 0.1 #646464, stop: 1 #5d5d5d);\n"
"}\n"
"\n"
"QWidget:focus\n"
"{\n"
"    /*border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);*/\n"
"}\n"
"\n"
"QLineEdit\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #4d4d4d, stop: 0 #646464, stop: 1 #5d5d5d);\n"
"    padding: 1px;\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"}\n"
"\n"
"QPushButton\n"
""
                        "{\n"
"    color: #b1b1b1;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-width: 1px;\n"
"    border-color: #1e1e1e;\n"
"    border-style: solid;\n"
"    border-radius: 6;\n"
"    padding: 3px;\n"
"    font-size: 12px;\n"
"    padding-left: 5px;\n"
"    padding-right: 5px;\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"}\n"
"\n"
"QComboBox\n"
"{\n"
"    selection-background-color: #ffaa00;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"}\n"
"\n"
"QComboBox:hover,QPushButton:hover\n"
"{\n"
"    border: 2px solid QLinear"
                        "Gradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"\n"
"QComboBox:on\n"
"{\n"
"    padding-top: 3px;\n"
"    padding-left: 4px;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"    selection-background-color: #ffaa00;\n"
"}\n"
"\n"
"QComboBox QAbstractItemView\n"
"{\n"
"    border: 2px solid darkgray;\n"
"    selection-background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QComboBox::drop-down\n"
"{\n"
"     subcontrol-origin: padding;\n"
"     subcontrol-position: top right;\n"
"     width: 15px;\n"
"\n"
"     border-left-width: 0px;\n"
"     border-left-color: darkgray;\n"
"     border-left-style: solid; /* just a single line */\n"
"     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
"     border-bottom-right-radius: 3px;\n"
" }\n"
"\n"
"QComboBox::down-arrow\n"
"{\n"
"     imag"
                        "e: url(:/down_arrow.png);\n"
"}\n"
"\n"
"QGroupBox:focus\n"
"{\n"
"border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QTextEdit:focus\n"
"{\n"
"    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"}\n"
"\n"
"QScrollBar:horizontal {\n"
"     border: 1px solid #222222;\n"
"     background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"     height: 7px;\n"
"     margin: 0px 16px 0 16px;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 0.5 #d7801a, stop: 1 #ffa02f);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      width:"
                        " 14px;\n"
"      subcontrol-position: right;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      width: 14px;\n"
"     subcontrol-position: left;\n"
"     subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::right-arrow:horizontal, QScrollBar::left-arrow:horizontal\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QScrollBar:vertical\n"
"{\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0, stop: 0.0 #121212, stop: 0.2 #282828, stop: 1 #484848);\n"
"      width: 7px;\n"
"      margin: 16px 0 16px 0;\n"
"      border: 1px solid #222222;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical\n"
"{\n"
"      b"
                        "ackground: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 0.5 #d7801a, stop: 1 #ffa02f);\n"
"      min-height: 20px;\n"
"      border-radius: 2px;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffa02f, stop: 1 #d7801a);\n"
"      height: 14px;\n"
"      subcontrol-position: bottom;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical\n"
"{\n"
"      border: 1px solid #1b1b19;\n"
"      border-radius: 2px;\n"
"      background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #d7801a, stop: 1 #ffa02f);\n"
"      height: 14px;\n"
"      subcontrol-position: top;\n"
"      subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical\n"
"{\n"
"      border: 1px solid black;\n"
"      width: 1px;\n"
"      height: 1px;\n"
"      background: white;\n"
"}\n"
"\n"
"\n"
"QScroll"
                        "Bar::add-page:vertical, QScrollBar::sub-page:vertical\n"
"{\n"
"      background: none;\n"
"}\n"
"\n"
"QTextEdit\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QPlainTextEdit\n"
"{\n"
"    background-color: #242424;\n"
"}\n"
"\n"
"QHeaderView::section\n"
"{\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050, stop: 0.6 #434343, stop:1 #656565);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QCheckBox:disabled\n"
"{\n"
"color: #414141;\n"
"}\n"
"\n"
"QDockWidget::title\n"
"{\n"
"    text-align: center;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #323232, stop: 0.5 #242424, stop:1 #323232);\n"
"}\n"
"\n"
"QDockWidget::close-button, QDockWidget::float-button\n"
"{\n"
"    text-align: center;\n"
"    spacing: 1px; /* spacing between items in the tool bar */\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:"
                        "0, y2:1, stop:0 #323232, stop: 0.5 #242424, stop:1 #323232);\n"
"}\n"
"\n"
"QDockWidget::close-button:hover, QDockWidget::float-button:hover\n"
"{\n"
"    background: #242424;\n"
"}\n"
"\n"
"QDockWidget::close-button:pressed, QDockWidget::float-button:pressed\n"
"{\n"
"    padding: 1px -1px -1px 1px;\n"
"}\n"
"\n"
"QMainWindow::separator\n"
"{\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #161616, stop: 0.5 #151515, stop: 0.6 #212121, stop:1 #343434);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #4c4c4c;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QMainWindow::separator:hover\n"
"{\n"
"\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #d7801a, stop:0.5 #b56c17 stop:1 #ffa02f);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #6c6c6c;\n"
"    spacing: 3px; /* spacing between items in the tool bar */\n"
"}\n"
"\n"
"QToolBar::handle\n"
"{\n"
"     spacing: 3px; /* spacing"
                        " between items in the tool bar */\n"
"     background: url(:/images/handle.png);\n"
"}\n"
"\n"
"QMenu::separator\n"
"{\n"
"    height: 2px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:0 #161616, stop: 0.5 #151515, stop: 0.6 #212121, stop:1 #343434);\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    margin-left: 10px;\n"
"    margin-right: 5px;\n"
"}\n"
"\n"
"QProgressBar\n"
"{\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk\n"
"{\n"
"    background-color: #d7801a;\n"
"    width: 2.15px;\n"
"    margin: 0.5px;\n"
"}\n"
"\n"
"QTabBar::tab {\n"
"    color: #b1b1b1;\n"
"    border: 1px solid #444;\n"
"    border-bottom-style: none;\n"
"    background-color: #323232;\n"
"    padding-left: 10px;\n"
"    padding-right: 10px;\n"
"    padding-top: 3px;\n"
"    padding-bottom: 2px;\n"
"    margin-right: -1px;\n"
"}\n"
"\n"
"QTabWidget::pane {\n"
"    border: 1px solid #444;\n"
"    top: 1px;\n"
"}\n"
"\n"
"QTabBa"
                        "r::tab:last\n"
"{\n"
"    margin-right: 0; /* the last selected tab has nothing to overlap with on the right */\n"
"    border-top-right-radius: 3px;\n"
"}\n"
"\n"
"QTabBar::tab:first:!selected\n"
"{\n"
" margin-left: 0px; /* the last selected tab has nothing to overlap with on the right */\n"
"\n"
"\n"
"    border-top-left-radius: 3px;\n"
"}\n"
"\n"
"QTabBar::tab:!selected\n"
"{\n"
"    color: #b1b1b1;\n"
"    border-bottom-style: solid;\n"
"    margin-top: 3px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:1 #212121, stop:.4 #343434);\n"
"}\n"
"\n"
"QTabBar::tab:selected\n"
"{\n"
"    border-top-left-radius: 3px;\n"
"    border-top-right-radius: 3px;\n"
"    margin-bottom: 0px;\n"
"}\n"
"\n"
"QTabBar::tab:!selected:hover\n"
"{\n"
"    /*border-top: 2px solid #ffaa00;\n"
"    padding-bottom: 3px;*/\n"
"    border-top-left-radius: 3px;\n"
"    border-top-right-radius: 3px;\n"
"    background-color: QLinearGradient(x1:0, y1:0, x2:0, y2:1, stop:1 #212121, stop:0.4 #343434, stop:0.2 #3434"
                        "34, stop:0.1 #ffaa00);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked, QRadioButton::indicator:unchecked{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"    border: 1px solid #b1b1b1;\n"
"    border-radius: 6px;\n"
"}\n"
"\n"
"QRadioButton::indicator:checked\n"
"{\n"
"    background-color: qradialgradient(\n"
"        cx: 0.5, cy: 0.5,\n"
"        fx: 0.5, fy: 0.5,\n"
"        radius: 1.0,\n"
"        stop: 0.25 #ffaa00,\n"
"        stop: 0.3 #323232\n"
"    );\n"
"}\n"
"\n"
"QCheckBox::indicator{\n"
"    color: #b1b1b1;\n"
"    background-color: #323232;\n"
"    border: 1px solid #b1b1b1;\n"
"    width: 9px;\n"
"    height: 9px;\n"
"}\n"
"\n"
"QRadioButton::indicator\n"
"{\n"
"    border-radius: 6px;\n"
"}\n"
"QCheckBox::indicator:checked\n"
"{\n"
"	 image: url(:/NosmallEditor/checked-new.png);\n"
"}\n"
"QRadioButton::indicator:hover, QCheckBox::indicator:hover\n"
"{\n"
"    border: 1px solid #ffaa00;\n"
"}\n"
"\n"
"QCheckBox::indicator:disabled, QRadioButton::indicator:disabled\n"
"{\n"
"    "
                        "border: 1px solid #444;\n"
"}\n"
"\n"
"QTreeView::item:selected {\n"
"    background-color: #1d3dec;\n"
"    color: white;\n"
"}\n"
"\n"
"QTreeView::item:open:has-children\n"
"{\n"
"	image: url(:/NosmallEditor/branch-open.png);\n"
"	image-position: middle right;\n"
"}\n"
"\n"
"QTreeView::item:closed:has-children\n"
"{\n"
"	image: url(:/NosmallEditor/branch-closed.png);\n"
"	image-position: middle right;\n"
"}\n"
"\n"
""));
        NosmallEditorClass->setIconSize(QSize(26, 27));
        NosmallEditorClass->setToolButtonStyle(Qt::ToolButtonIconOnly);
        NosmallEditorClass->setTabShape(QTabWidget::Rounded);
        actionOpen = new QAction(NosmallEditorClass);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSave = new QAction(NosmallEditorClass);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSave->setEnabled(false);
        action = new QAction(NosmallEditorClass);
        action->setObjectName(QString::fromUtf8("action"));
        actionReload = new QAction(NosmallEditorClass);
        actionReload->setObjectName(QString::fromUtf8("actionReload"));
        actionReload->setEnabled(false);
        actionLoad_Nosmall = new QAction(NosmallEditorClass);
        actionLoad_Nosmall->setObjectName(QString::fromUtf8("actionLoad_Nosmall"));
        action_3 = new QAction(NosmallEditorClass);
        action_3->setObjectName(QString::fromUtf8("action_3"));
        actionTest = new QAction(NosmallEditorClass);
        actionTest->setObjectName(QString::fromUtf8("actionTest"));
        actionLoad = new QAction(NosmallEditorClass);
        actionLoad->setObjectName(QString::fromUtf8("actionLoad"));
        actionSave_2 = new QAction(NosmallEditorClass);
        actionSave_2->setObjectName(QString::fromUtf8("actionSave_2"));
        actionLoad_2 = new QAction(NosmallEditorClass);
        actionLoad_2->setObjectName(QString::fromUtf8("actionLoad_2"));
        actionSave_3 = new QAction(NosmallEditorClass);
        actionSave_3->setObjectName(QString::fromUtf8("actionSave_3"));
        actiontest = new QAction(NosmallEditorClass);
        actiontest->setObjectName(QString::fromUtf8("actiontest"));
        centralWidget = new QWidget(NosmallEditorClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        treeWidget = new QTreeWidget(centralWidget);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setGeometry(QRect(10, 120, 111, 221));
        treeWidget->setStyleSheet(QString::fromUtf8(""));
        treeWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        treeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        treeWidget->setIndentation(0);
        treeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(230, 100, 141, 31));
        lineEdit->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
" border: 0;\n"
" border-bottom: 1px solid #2980b9;\n"
" color: #2980b9;\n"
" background: transparent;\n"
"padding: 4px;\n"
"}"));
        lineEdit->setMaxLength(20);
        lineEdit->setAlignment(Qt::AlignCenter);
        lineEdit->setReadOnly(true);
        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(130, 247, 461, 20));
        lineEdit_2->setStyleSheet(QString::fromUtf8(""));
        lineEdit_2->setMaxLength(20);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(340, 70, 158, 29));
        label_2->setStyleSheet(QString::fromUtf8("background-color: #323232;"));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/NosmallEditor/Test.png")));
        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setGeometry(QRect(170, 330, 91, 22));
        spinBox->setStyleSheet(QString::fromUtf8(""));
        spinBox->setMinimum(1);
        spinBox->setMaximum(9999999);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(130, 333, 31, 16));
        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(519, 120, 71, 16));
        checkBox->setCheckable(true);
        checkBox->setChecked(false);
        checkBox_2 = new QCheckBox(centralWidget);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setGeometry(QRect(520, 90, 70, 17));
        checkBox_2->setStyleSheet(QString::fromUtf8(""));
        checkBox_2->setCheckable(true);
        checkBox_2->setChecked(false);
        comboBox = new QComboBox(centralWidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setEnabled(false);
        comboBox->setGeometry(QRect(320, 330, 111, 22));
        QFont font;
        font.setStyleStrategy(QFont::PreferDefault);
        comboBox->setFont(font);
        comboBox->setStyleSheet(QString::fromUtf8(""));
        comboBox->setLocale(QLocale(QLocale::German, QLocale::Germany));
        comboBox->setEditable(false);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(278, 333, 31, 16));
        lineEdit_3 = new QLineEdit(centralWidget);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(130, 290, 461, 20));
        lineEdit_3->setStyleSheet(QString::fromUtf8(""));
        lineEdit_3->setMaxLength(150);
        lcdNumber = new QLCDNumber(centralWidget);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        lcdNumber->setGeometry(QRect(52, 343, 51, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font1.setUnderline(false);
        lcdNumber->setFont(font1);
        lcdNumber->setStyleSheet(QString::fromUtf8("background-color: #323232;"));
        lcdNumber->setSmallDecimalPoint(false);
        lcdNumber->setDigitCount(5);
        lcdNumber->setSegmentStyle(QLCDNumber::Flat);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(11, 346, 31, 16));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(0, 0, 601, 31));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy);
        label_6->setStyleSheet(QString::fromUtf8("QLabel {\n"
"\n"
"text-align: center;\n"
" border: 1px;\n"
" border-bottom: 1px solid black;\n"
" color: black;\n"
" background: grey;\n"
"padding: 4px;\n"
"}"));
        label_6->setTextFormat(Qt::PlainText);
        label_6->setAlignment(Qt::AlignCenter);
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(567, 3, 31, 23));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(530, 3, 31, 23));
        lineEdit_5 = new QLineEdit(centralWidget);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(10, 93, 111, 20));
        lineEdit_5->setStyleSheet(QString::fromUtf8(""));
        lineEdit_5->setMaxLength(20);
        widget = new MyWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 30, 601, 22));
        widget->setStyleSheet(QString::fromUtf8(""));
        checkBox_3 = new QCheckBox(centralWidget);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(520, 60, 70, 17));
        checkBox_3->setStyleSheet(QString::fromUtf8(""));
        checkBox_3->setCheckable(true);
        checkBox_3->setChecked(false);
        plainTextEdit = new QPlainTextEdit(centralWidget);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setEnabled(false);
        plainTextEdit->setGeometry(QRect(10, 390, 581, 361));
        plainTextEdit->setStyleSheet(QString::fromUtf8("border: 0px;"));
        plainTextEdit->setLocale(QLocale(QLocale::Chinese, QLocale::China));
        plainTextEdit->setReadOnly(false);
        lineEdit_4 = new QLineEdit(centralWidget);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(130, 207, 461, 20));
        lineEdit_4->setStyleSheet(QString::fromUtf8(""));
        lineEdit_4->setMaxLength(20);
        lineEdit_4->setReadOnly(true);
        lineEdit_6 = new QLineEdit(centralWidget);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(130, 170, 461, 20));
        lineEdit_6->setStyleSheet(QString::fromUtf8(""));
        lineEdit_6->setMaxLength(20);
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(150, 110, 36, 36));
        label_7->setStyleSheet(QString::fromUtf8("background-color: #323232;"));
        lineEdit_7 = new QLineEdit(centralWidget);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));
        lineEdit_7->setGeometry(QRect(450, 120, 61, 20));
        lineEdit_7->setStyleSheet(QString::fromUtf8(""));
        lineEdit_7->setMaxLength(20);
        lineEdit_7->setReadOnly(true);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(300, 150, 47, 13));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(300, 190, 47, 13));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(300, 228, 71, 16));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(298, 270, 91, 16));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(400, 124, 47, 13));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(450, 333, 41, 20));
        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        spinBox_2->setGeometry(QRect(500, 330, 91, 22));
        spinBox_2->setStyleSheet(QString::fromUtf8(""));
        spinBox_2->setReadOnly(true);
        spinBox_2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_2->setMinimum(1);
        spinBox_2->setMaximum(99);
        checkBox_4 = new QCheckBox(centralWidget);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));
        checkBox_4->setGeometry(QRect(520, 146, 71, 16));
        checkBox_4->setCheckable(true);
        checkBox_4->setChecked(false);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(10, 64, 141, 23));
        checkBox_5 = new QCheckBox(centralWidget);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));
        checkBox_5->setGeometry(QRect(250, 70, 81, 17));
        NosmallEditorClass->setCentralWidget(centralWidget);
        label_6->raise();
        treeWidget->raise();
        lineEdit->raise();
        lineEdit_2->raise();
        label_2->raise();
        spinBox->raise();
        label_3->raise();
        checkBox->raise();
        checkBox_2->raise();
        comboBox->raise();
        label_4->raise();
        lineEdit_3->raise();
        lcdNumber->raise();
        label_5->raise();
        pushButton_3->raise();
        pushButton_4->raise();
        lineEdit_5->raise();
        widget->raise();
        checkBox_3->raise();
        plainTextEdit->raise();
        lineEdit_4->raise();
        lineEdit_6->raise();
        label_7->raise();
        lineEdit_7->raise();
        label->raise();
        label_8->raise();
        label_9->raise();
        label_10->raise();
        label_11->raise();
        label_12->raise();
        spinBox_2->raise();
        checkBox_4->raise();
        pushButton->raise();
        checkBox_5->raise();

        retranslateUi(NosmallEditorClass);

        QMetaObject::connectSlotsByName(NosmallEditorClass);
    } // setupUi

    void retranslateUi(QMainWindow *NosmallEditorClass)
    {
        NosmallEditorClass->setWindowTitle(QApplication::translate("NosmallEditorClass", "Nosmall Editor", nullptr));
        actionOpen->setText(QApplication::translate("NosmallEditorClass", "Open", nullptr));
        actionSave->setText(QApplication::translate("NosmallEditorClass", "Save", nullptr));
        action->setText(QApplication::translate("NosmallEditorClass", "?", nullptr));
        actionReload->setText(QApplication::translate("NosmallEditorClass", "Reload", nullptr));
        actionLoad_Nosmall->setText(QApplication::translate("NosmallEditorClass", "Load Nosmall", nullptr));
        action_3->setText(QApplication::translate("NosmallEditorClass", "?", nullptr));
        actionTest->setText(QApplication::translate("NosmallEditorClass", "Test", nullptr));
        actionLoad->setText(QApplication::translate("NosmallEditorClass", "Load", nullptr));
        actionSave_2->setText(QApplication::translate("NosmallEditorClass", "Save", nullptr));
        actionLoad_2->setText(QApplication::translate("NosmallEditorClass", "Load", nullptr));
        actionSave_3->setText(QApplication::translate("NosmallEditorClass", "Save", nullptr));
        actiontest->setText(QApplication::translate("NosmallEditorClass", "test", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("NosmallEditorClass", "Nosmall Items", nullptr));
        lineEdit->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Id", nullptr));
        lineEdit_2->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Description", nullptr));
        label_2->setText(QString());
        label_3->setText(QApplication::translate("NosmallEditorClass", "Price:", nullptr));
        checkBox->setText(QApplication::translate("NosmallEditorClass", "Is Hot", nullptr));
        checkBox_2->setText(QApplication::translate("NosmallEditorClass", "Is New", nullptr));
        comboBox->setItemText(0, QApplication::translate("NosmallEditorClass", "All", nullptr));
        comboBox->setItemText(1, QApplication::translate("NosmallEditorClass", "Player", nullptr));
        comboBox->setItemText(2, QApplication::translate("NosmallEditorClass", "NosMate", nullptr));
        comboBox->setItemText(3, QApplication::translate("NosmallEditorClass", "Decorate", nullptr));
        comboBox->setItemText(4, QApplication::translate("NosmallEditorClass", "Single use", nullptr));

        label_4->setText(QApplication::translate("NosmallEditorClass", "Type", nullptr));
        lineEdit_3->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Item Description", nullptr));
        label_5->setText(QApplication::translate("NosmallEditorClass", "Items:", nullptr));
        label_6->setText(QApplication::translate("NosmallEditorClass", "Nosmall Editor", nullptr));
        pushButton_3->setText(QApplication::translate("NosmallEditorClass", "X", nullptr));
        pushButton_4->setText(QApplication::translate("NosmallEditorClass", "_", nullptr));
        lineEdit_5->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Search Item", nullptr));
        checkBox_3->setText(QApplication::translate("NosmallEditorClass", "Activated", nullptr));
        plainTextEdit->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Text", nullptr));
        lineEdit_4->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Vnum", nullptr));
        lineEdit_6->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Name", nullptr));
        label_7->setText(QString());
        lineEdit_7->setPlaceholderText(QApplication::translate("NosmallEditorClass", "Duration", nullptr));
        label->setText(QApplication::translate("NosmallEditorClass", "Name", nullptr));
        label_8->setText(QApplication::translate("NosmallEditorClass", "Vnum", nullptr));
        label_9->setText(QApplication::translate("NosmallEditorClass", "Description", nullptr));
        label_10->setText(QApplication::translate("NosmallEditorClass", "Item Description", nullptr));
        label_11->setText(QApplication::translate("NosmallEditorClass", "Duration", nullptr));
        label_12->setText(QApplication::translate("NosmallEditorClass", "Amount", nullptr));
        checkBox_4->setText(QApplication::translate("NosmallEditorClass", "Show", nullptr));
        pushButton->setText(QApplication::translate("NosmallEditorClass", "Show Activated Items", nullptr));
        checkBox_5->setText(QApplication::translate("NosmallEditorClass", "Is List Item", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NosmallEditorClass: public Ui_NosmallEditorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOSMALLEDITOR_H
