#ifndef NOSMALLEDITOR_H
#define NOSMALLEDITOR_H

#define player
#define nosmate
#define decorate
#define singleuse

#include <QtWidgets/QMainWindow>
#include <QMouseEvent>
#include "ui_nosmalleditor.h"

class NosmallEditor : public QMainWindow
{
	Q_OBJECT

public:
	NosmallEditor(QWidget *parent = 0);
	~NosmallEditor();
	bool selected = false;
	bool selectednew = false;
	bool LoadedFiles = false;
	bool LoadedNosmall = false;
	void LoadNosmallFile();
	void LoadNosmallItems();

protected:
	void mouseMoveEvent(QMouseEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;

private:
	struct NostaleFile {
		std::string FileName;
		int size;
		std::string Content;
		std::string name_end;
	};
	/*struct NosmallItem {
		std::string Name, Description, ItemDescription;
		std::size_t Vnum, Price, Type, ID;
		bool isHot, isNew, Activated;
	};*/
	struct NosmallItem {
		std::string Name, Description, ItemDescription;
		std::size_t Id, VNUM_1, VNUM_2;
		std::size_t ITEM_0, ITEM_1, PictureID, ITEM_2, ITEM_3;
		std::string  ItemVnum, ID_NUM;
		std::size_t Price, COST_0, COST_1, Amount, COST_3, Duration;
		std::size_t Count;
		std::string ITEM_ID_1, ITEM_ID_2, ITEM_ID_3, ITEM_ID_4, ITEM_ID_5;
		bool isHot, isNew, Activated, Show;
		bool isSub = false;
	};
	Ui::NosmallEditorClass ui;
	QPoint dragPosition;


	std::vector<NostaleFile> NTFiles;
	std::vector<NosmallItem*> NTItem;
	std::string NosTaleDataPath;

	//Menu
	QMenu *menu;
	QAction *Load;
	QAction *Save;
	QAction *CreateList;
	QAction *SaveFile;

private slots:
	void ChangedSelected();
	void NameChanged();
	void Search();
	void ButtonClose();
	void ButtonMinimize();
	void GetNostaleDataPath();
	void SaveList();
	void SaveDat();

};

#endif // NOSMALLEDITOR_H
