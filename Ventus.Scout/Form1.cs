﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenNos.GameObject;
using NosTale.Packets;
using Tulpep.NotificationWindow;
using OpenNos.GameObject.Networking;
using System.Threading;

namespace Ventus.Scout
{
    public partial class Form1 : Form
    {

        public ClientSession session { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.timer1.Start();
            menuStrip1.Hide();
            pictureBox1.Hide();
            button2.Hide();
            groupBox1.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.progressBar1.Increment(3);

            if (progressBar1.Value >= progressBar1.Maximum)
            {
                timer1.Stop();
                #region Popup
                PopupNotifier popup = new PopupNotifier();
                popup.TitleText = "Successful!";
                popup.ContentText = "The Connection to [NosTale Ventus] [DB: ventus] [Port: 4000] has been initialized";
                popup.TitleColor = Color.Green;
                popup.Popup();
                #endregion

                progressBar1.Hide();
                menuStrip1.Show();
                pictureBox1.Show();
                button2.Show();
                groupBox1.Show();
            }

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

     

        private void button2_Click(object sender, EventArgs e)
        {
            groupBox1.Hide();
            button2.Hide();
            Thread.Sleep(1000);
            button2.Show();
            groupBox1.Show();
        }
    }
}
