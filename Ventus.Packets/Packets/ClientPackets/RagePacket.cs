﻿using OpenNos.Core;

namespace NosTale.Packets.Packets.ClientPackets
{
    [PacketHeader("rage")]
    public class RagePacket : PacketDefinition
    {

        [PacketIndex(0)]
        public short Type { get; set; }

        [PacketIndex(1)]
        public short Value { get; set; }

    }
}