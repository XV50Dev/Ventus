﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rage2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "RageValue1", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "RageValue2", c => c.Int(nullable: false));
            DropColumn("dbo.Character", "Rage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Character", "Rage", c => c.Byte(nullable: false));
            DropColumn("dbo.Character", "RageValue2");
            DropColumn("dbo.Character", "RageValue1");
        }
    }
}
