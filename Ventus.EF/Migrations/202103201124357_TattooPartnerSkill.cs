﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TattooPartnerSkill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterSkill", "IsPartnerSkill", c => c.Boolean(nullable: false));
            DropColumn("dbo.CharacterSkill", "TattooBuff");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CharacterSkill", "TattooBuff", c => c.Int(nullable: false));
            DropColumn("dbo.CharacterSkill", "IsPartnerSkill");
        }
    }
}
