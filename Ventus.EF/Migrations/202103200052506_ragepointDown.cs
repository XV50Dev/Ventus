﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ragepointDown : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Character", "RagePoint");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Character", "RagePoint", c => c.Long(nullable: false));
        }
    }
}
