﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ns3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterSkill", "TattooBuff", c => c.Int(nullable: false));
            DropColumn("dbo.Item", "IsDestroyable");
            DropColumn("dbo.CharacterSkill", "IsPartnerSkill");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CharacterSkill", "IsPartnerSkill", c => c.Boolean(nullable: false));
            AddColumn("dbo.Item", "IsDestroyable", c => c.Boolean(nullable: false));
            DropColumn("dbo.CharacterSkill", "TattooBuff");
        }
    }
}
