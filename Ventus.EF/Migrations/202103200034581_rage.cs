﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "Rage", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Character", "Rage");
        }
    }
}
